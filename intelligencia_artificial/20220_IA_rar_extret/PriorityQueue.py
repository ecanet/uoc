import heapq


class PriorityQueue:
    def __init__(self):
        self.__elements = []

    def empty(self):
        return len(self.__elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.__elements, (priority, item))

    def get(self):
        return heapq.heappop(self.__elements)[1]

    def __str__(self):
        cadena = "["
        for pes, estat in self.__elements:
            cadena += estat + "("+str(pes)+") "
        return cadena + "]"