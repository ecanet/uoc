;; (load "astar.lisp")

;;;;;;;;;;;;; quicksort ;;;;;;;;;;;;

(defun selecciona-estimacio (unio-nodes)
  (mapcar #'(lambda (node) (caddr (cdddr node)))
            unio-nodes))

(defun selecciona-menorigual (pivot per-ordenar elems)
  (if (null per-ordenar) nil
    (if (<= (car per-ordenar) pivot)
        (cons (car elems)
              (selecciona-menorigual pivot (cdr per-ordenar)
                                     (cdr elems)))
      (selecciona-menorigual pivot (cdr per-ordenar) (cdr elems))))) 

(defun selecciona-major (pivot per-ordenar elems)
  (if (null per-ordenar) nil
    (if (> (car per-ordenar) pivot)
        (cons (car elems)
              (selecciona-major pivot (cdr per-ordenar)
                                (cdr elems))) 
      (selecciona-major pivot (cdr per-ordenar) (cdr elems)))))

(defun quicksort (per-ordenar elems)
  (if (null elems) nil
    (let* ((pivot (car per-ordenar)) (elemp (car elems))
           (petits (selecciona-menorigual pivot (cdr per-ordenar) (cdr elems))) 
           (grans (selecciona-major pivot (cdr per-ordenar) (cdr elems)))
           (resultat  (append (quicksort
                               (selecciona-estimacio petits)
                               petits)
                              (cons elemp (quicksort
                                           (selecciona-estimacio grans) grans)))))
      resultat)))

;;;;;;;;;;;;; quicksort ;;;;;;;;;;;;

;;;;;;;;;;;;;   node    ;;;;;;;;;;;;

(defun id (node) (car node))

(defun estat (node) (cadr node))

(defun id-pare (node) (caddr node))

(defun operador (node) (cadddr node))

(defun info (node) (cddddr node))

(defun construeix-node (id estat id-pare op info) 
  (append (list id estat id-pare op) info))

(defun expandeix-node (node operadors funcio)
  (labels ((elimina-estats-buits (llista-nodes)
                                 (remove-if #'(lambda (node)
                                                (equal (estat node) 'buit))
                                            llista-nodes)))
    (let ((estat   (estat node))
          (id-node (id node))
          (info    (info node)))
      (elimina-estats-buits
       (mapcar #'(lambda (operador)
                   (construeix-node (gensym)
                                    (funcall (cadr operador) estat info)
                                    id-node
                                    (car operador)
                                    (funcall funcio (list estat info) (funcall (cadr operador) estat info) (car operador))))
               operadors)))))                   ;;; aquí ^ l'original passa només info com a paràmetre

;;;;;;;;;;;;;   node    ;;;;;;;;;;;;

;;;;;;;;;;;;; problema  ;;;;;;;;;;;;

(defun operadors (problema) (car problema))

(defun funcio-info-addicional (problema) (cadr problema))

(defun estat-inicial (problema) (caddr problema))

(defun funcio-objectiu (problema) (cadddr problema))

(defun info-inicial (problema) (car (cddddr problema)))

(defun solucio? (problema node) (funcall (funcio-objectiu problema) (estat node)))

;;;;;;;;;;;;; problema  ;;;;;;;;;;;;

;;;;;;;;;;;;;   arbre   ;;;;;;;;;;;;

(defun nodes-a-expandir (arbre)
  (car arbre))

(defun nodes-expandits (arbre)
  (cadr arbre))

(defun selecciona-node (arbre)
  (car (nodes-a-expandir arbre)))

(defun candidats? (arbre)
  (not (null (nodes-a-expandir arbre))))

(defun cami (arbre node)
  (if (null (id-pare node)) nil
    (append (cami arbre
                  (node-arbre (id-pare node) arbre))
            (list (operador node)))))

(defun node-arbre (id-node arbre)
  (let ((a-expandir? (member-if #'(lambda (node) (equal (id node) id-node)) (nodes-a-expandir arbre))))
    (if a-expandir? 
        (find-if #'(lambda (node) (equal (id node) id-node))
                 (nodes-a-expandir arbre)) 
      (find-if #'(lambda (node) (equal (id node) id-node))
               (nodes-expandits arbre)))))

(defun construeix-arbre (arbre estrategia node-expandit nous-nodes-a-expandir)
  (cons (funcall estrategia (car arbre) nous-nodes-a-expandir)
        (list (cons node-expandit (cadr arbre)))))

(defun expandeix-arbre (problema estrategia arbre node)
  (let ((nous-nodes-a-expandir (expandeix-node node
                                               (operadors problema)
                                               (funcio-info-addicional problema)) ))
    (construeix-arbre arbre estrategia node nous-nodes-a-expandir)))

(defun elimina-seleccio (arbre)
  (cons (cdr (nodes-a-expandir arbre))
        (cdr arbre)))

(defun arbre-inicial (estat info)
  (list (list (append (construeix-node (gensym) estat nil nil nil) (funcall info estat)))))

;;;;;;;;;;;;;   arbre   ;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun cerca (problema estrategia arbre)
  (if (not (candidats? arbre)) '(no-hi-ha-solucio)
    (let ((node (selecciona-node arbre))
          (nou-arbre (elimina-seleccio arbre)))
      ;;;(format t "~{~a~^  ~}~%" (map 'list #'(lambda (node) (list (nth 1 node) (nth 5 node))) (car arbre)))
      ;;;(format t "~{~a~^  ~}~%" (map 'list #'(lambda (node) (list (nth 1 node) (nth 5 node))) (cadr arbre)))
      ;;;(format t "~%")      
      (if (solucio? problema node)
          (cami arbre node)
        (cerca problema estrategia (expandeix-arbre problema estrategia nou-arbre node))))))

(defun fer-cerca (problema estrategia)
  (cerca problema estrategia (arbre-inicial (estat-inicial problema)
                                            (info-inicial problema))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;; cerca A*

;;;;;;;;; Definició del graf:
;;;;;;;;;
;;;;;;;;; Un operador per a cada aresta del graf

(defun AtoB (estat info)
  (if (equal estat 'A) 'B 'buit))
(defun AtoC (estat info)
  (if (equal estat 'A) 'C 'buit))
(defun AtoD (estat info)
  (if (equal estat 'A) 'D 'buit))
(defun BtoD (estat info)
  (if (equal estat 'B) 'D 'buit))
(defun BtoG (estat info)
  (if (equal estat 'B) 'G 'buit))
(defun CtoB (estat info)
  (if (equal estat 'C) 'B 'buit))
(defun CtoE (estat info)
  (if (equal estat 'C) 'E 'buit))
(defun DtoE (estat info)
  (if (equal estat 'D) 'E 'buit))
(defun EtoF (estat info)
  (if (equal estat 'E) 'F 'buit))
(defun EtoH (estat info)
  (if (equal estat 'E) 'H 'buit))
(defun FtoG (estat info)
  (if (equal estat 'F) 'G 'buit))
(defun GtoH (estat info)
  (if (equal estat 'G) 'H 'buit))
(defun HtoI (estat info)
  (if (equal estat 'H) 'I 'buit))
(defun ItoC (estat info)
  (if (equal estat 'I) 'C 'buit))

(defvar tl-operadors
  (list (list 'AtoB #'AtoB)
        (list 'AtoC #'AtoC)
        (list 'AtoD #'AtoD)
        (list 'BtoD #'BtoD)
        (list 'BtoG #'BtoG)
        (list 'CtoB #'CtoB)
        (list 'CtoE #'CtoE)
        (list 'DtoE #'DtoE)
        (list 'EtoF #'EtoF)
        (list 'EtoH #'EtoH)
        (list 'FtoG #'FtoG)
        (list 'GtoH #'GtoH)
        (list 'HtoI #'HtoI)
        (list 'ItoC #'ItoC)))

;;;;;;;;; Funció de cost

(defun cost (estat1 estat2)
  (cond ((and (equal estat1 'A) (equal estat2 'B))  20)
        ((and (equal estat1 'A) (equal estat2 'C))  20)
        ((and (equal estat1 'A) (equal estat2 'D))   1)
        ((and (equal estat1 'B) (equal estat2 'D))   4)
        ((and (equal estat1 'B) (equal estat2 'G))   4)
        ((and (equal estat1 'C) (equal estat2 'B))   3)
        ((and (equal estat1 'C) (equal estat2 'E))   3)
        ((and (equal estat1 'D) (equal estat2 'E))   5)
        ((and (equal estat1 'E) (equal estat2 'F))   9)
        ((and (equal estat1 'E) (equal estat2 'H))   2)
        ((and (equal estat1 'F) (equal estat2 'G))  10)
        ((and (equal estat1 'G) (equal estat2 'H))   4)
        ((and (equal estat1 'H) (equal estat2 'I))   1)
        ((and (equal estat1 'I) (equal estat2 'C))   7)
        (t                                         100)))

;;;;;;;;; Funció heurística

;;; L'heurística del graf de l'enunciat
;;; (defun heuristica (estat)
;;;    (cond ((equal estat 'A)    15)
;;;          ((equal estat 'B)     2)
;;;          ((equal estat 'C)    15)
;;;          ((equal estat 'D)    18)
;;;          ((equal estat 'E)     7)
;;;          ((equal estat 'F)     5)
;;;          ((equal estat 'G)     0)
;;;          ((equal estat 'H)     9)
;;;          ((equal estat 'I)     2)
;;;          (t                  100)))

;;; L'heurística del graf de la solució
(defun heuristica (estat)
   (cond ((equal estat 'A)    2)
         ((equal estat 'B)    1)
         ((equal estat 'C)    3)
         ((equal estat 'D)    5)
         ((equal estat 'E)    1)
         ((equal estat 'F)    3)
         ((equal estat 'G)    0)
         ((equal estat 'H)    2)
         ((equal estat 'I)    5)
         (t                 100)))

;;;;;;;;; Algorismes generals cerca

(defun tl-estrategia-cost-uniforme (nodes-a-expandir nous-nodes-a-expandir)  ;;; El mateix que tl-estrategia-A* !!! 
  (let ((unio-nodes (append nous-nodes-a-expandir
                            nodes-a-expandir)))
    (quicksort (selecciona-estimacio unio-nodes)
               unio-nodes)))

(defun tl-estrategia-avida (nodes-a-expandir nous-nodes-a-expandir)  ;;; El mateix que tl-estrategia-A* !!! 
  (let ((unio-nodes (append nous-nodes-a-expandir
                            nodes-a-expandir)))
    (quicksort (selecciona-estimacio unio-nodes)
               unio-nodes)))

(defun tl-estrategia-A* (nodes-a-expandir nous-nodes-a-expandir) 
  (let ((unio-nodes (append nous-nodes-a-expandir
                            nodes-a-expandir)))
    (quicksort (selecciona-estimacio unio-nodes)
               unio-nodes)))

(defvar problema-cerca-cost-uniforme                       ;;; El mateix que problema-cerca-A* amb h=0 sempre !!!
  (list tl-operadors
        #'(lambda (info-node-pare estat nom-operador)      ;;; *** info-node-pare = (estat-pare (g g-plus-h))
            (let ((estat-pare  (car info-node-pare))       ;;; Això també és diferent de l'original
                  (g           (caadr info-node-pare))
                  (g-plus-h    (cadadr info-node-pare)))
              (list (+ g (cost estat-pare estat))
                    (+ (+ g (cost estat-pare estat)) 0)))) ;;; sumem zero, l'heurística en el cost uniforme!!
        'A
        #'(lambda (estat) (equal estat 'G))
        #'(lambda (estat) (list 0 0)) ))                   ;;; L'heurística de l'estat inicial... 0!

(defvar problema-cerca-avida                               ;;; El mateix que problema-cerca-A* amb g(n)=0 sempre !!!
  (list tl-operadors
        #'(lambda (info-node-pare estat nom-operador)      ;;; *** info-node-pare = (estat-pare (g g-plus-h))
            (list 0 (heuristica estat)))
        'A
        #'(lambda (estat) (equal estat 'G))
        #'(lambda (estat) (list 0 (heuristica estat))) ))

(defvar problema-cerca-A*
  (list tl-operadors
        #'(lambda (info-node-pare estat nom-operador) ;;; *** info-node-pare = (estat-pare (g g-plus-h))
            (let ((estat-pare  (car info-node-pare))  ;;; Això també és diferent de l'original
                  (g           (caadr info-node-pare))
                  (g-plus-h    (cadadr info-node-pare)))
              (list (+ g (cost estat-pare estat))
                    (+ (+ g (cost estat-pare estat)) (heuristica estat)))))
        'A
        #'(lambda (estat) (equal estat 'G))
        #'(lambda (estat) (list 0 (heuristica estat))) ))

(defun cerca-cost-uniforme (problema)
  (fer-cerca problema #'tl-estrategia-cost-uniforme))

(defun cerca-avida (problema)
  (fer-cerca problema #'tl-estrategia-avida))

(defun cerca-A* (problema)
  (fer-cerca problema #'tl-estrategia-A*))


