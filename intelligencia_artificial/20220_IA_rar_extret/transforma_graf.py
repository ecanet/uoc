import sys
from easyinput import read

def llegir_graf_etiquetat(f):
    n = read(int,file=f)   # nombre de vertexos |V|
    m = read(int,file=f)   # nombre d'arestes |E|
    G = [None] * n
    for i in range(n):
        G[i] = []
    for i in range(m):     # m parelles u,v,w: aresta u --w--> v
        u = read(int,file=f)
        v = read(int,file=f)
        w = read(int,file=f)
        G[u].append((w,v))
    return G

fitxer = sys.argv[1]  # Hem de passar el nom del fitxer com a argument
                      # de la crida, per exemple:
                      # python3 dijkstra.py exemple_dijkstra1.inp
with open(fitxer) as f:
    G = llegir_graf_etiquetat(f)

for u in range(len(G)):
    for w,v in G[u]:
        print("def f",u,"to",v,"(estat,info): return ('",v,"' if estat == '",u,"' else 'buit')",sep='')
print()
print("def tl_operadors_graf():")
print("    return [")
for u in range(len(G)):
    for w,v in G[u]:
        print("            ['f",u,"to",v,"', f",u,"to",v,"]," ,sep='')
print("]")
print()
print("def cost(estat1,estat2):")
for u in range(len(G)):
    for w,v in G[u]:
        print("    elif estat1 == '",u,"' and estat2 == '",v,"': return ",w,sep='')
print("    else: return 100")
print()
print("def heuristic(estat):")
for u in range(len(G)):
    print("    elif estat1 == '",u,"': return ",sep='')
print("    else: return 100")

        
