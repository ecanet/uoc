.data
N:  .word 5
V:  .word 40,50,20,30,10 
aux:.word 0
.text
main:
       la   t1, N            # t1 <- @N
       la   t0, V            # t0 <- V
       lw   t1, 0(t1)        # t1 <- N
        addi t2, x0, 4        # t2 <- sizeof(int)
       mul t1, t1, t2
       addi s2, x0, 0        # s2=i <- 0
       sub  t5, t1, t2       # t5 <- (N*4)-4  
fori:  
       addi s3, x0, 0        # s3=j <- 0
forj:  
       beq  s3, t5, end_forj
       add  s4, s3, t0       # s4 <- *(V+j)
       lw   s6, 0(s4)        # s6 <- V[j]
       lw   s7, 4(s4)        # s7 <- V[j+1]
       bge  s7, s6, end_if
       sw   s7, 0(s4)        # V[j] = V[j+1]
       sw   s6, 4(s4)        #  V[j+1] = V[j]
end_if:
       addi s3, s3, 4        # j++
       bne  s3, t5, forj
end_forj:
       addi s2, s2, 4        # i++
       bne  s2, t5, fori
end_fori:
       nop
