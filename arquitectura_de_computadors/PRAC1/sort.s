#
# Sort RISC-V
#
.data
N:  .word 5
V:  .word 40,50,20,30,10 
aux:.word 0
.text
main:
       la   t0, V            # t0 <- V
       la   t1, N            # t1 <- @N
       lw   t1, 0(t1)        # t1 <- N
       addi t2, x0, 4        # t2 <- sizeof(int)
       addi t3, x0, 1        # t3 <- 1
       la   t4, aux          # t4 <- aux
       sub  t5, t1, t3       # t5 <- N-1
       addi s2, x0, 0        # s2=i <- 0
fori:  
       beq  s2, t5, end_fori
       addi s3, x0, 0        # s3=j <- 0
forj:  
       beq  s3, t5, end_forj
       mul  s4, s3, t2       # s4 <- (j * 4)
       add  s5, s4, t2       # s5 <- (j * 4) + 4
       add  s4, s4, t0       # s4 <- *(V+j)
       lw   s6, 0(s4)        # s6 <- V[j]
       add  s5, s5, t0       # s5 <- *(V+j+1)
       lw   s7, 0(s5)        # s7 <- V[j+1]
       bge  s7, s6, end_if
       sw   s6, 0(t4)        # aux = V[j]
       sw   s7, 0(s4)        # V[j] = V[j+1]
       lw   s6, 0(t4)        # 
       sw   s6, 0(s5)        # V[j+1] = aux
end_if:
       addi s3, s3, 1        # j++
       j    forj
end_forj:
       addi s2, s2, 1        # i++
       j    fori
end_fori:
       nop







