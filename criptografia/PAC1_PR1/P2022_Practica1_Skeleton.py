#!/usr/bin/env python3
# -*- coding: utf-8 -*-



# --- IMPLEMENTATION GOES HERE -----------------------------------------------
#  Student helpers (functions, constants, etc.) can be defined here, if needed



# ----------------------------------------------------------------------------





def uoc_simple_subs_cipher(message, shift):
    """
    EXERCISE 1.1: Simple substitution cipher
    :message: message to cipher (plaintext)
    :shift: offset or displacement
    :return: ciphered text
    """

    ciphertext = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return ciphertext


def uoc_simple_subs_decipher(message, shift):
    """
    EXERCISE 1.2: Simple substitution decipher
    :message: message to cipher (plaintext)
    :shift: offset or displacement
    :return: ciphered text
    """

    plaintext = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return plaintext




def uoc_mono_subs_cipher(message, subs_alphabet):
    """
    EXERCISE 1.3: Monoalphabetic substitution cipher
    :message: message to cipher (plaintext)
    :subs_alphabet: substitution alphabet
    :return: ciphered text
    """

    ciphertext = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return ciphertext


def uoc_mono_subs_decipher(message, subs_alphabet):
    """
    EXERCISE 1.4: Monoalphabetic substitution decipher
    :message: message to cipher (plaintext)
    :subs_alphabet: substitution alphabet
    :return: ciphered text
    """

    plaintext = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return plaintext



def uoc_find_shift(message):
    """
    EXERCISE 2.1: Find shift used by the simple substitution
    :message: encrypted message
    :return: estimated shift value
    """

    shift = -1

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return shift


def uoc_find_alphabet(message):
    """
    EXERCISE 2.2: Find or approximate the used substitution alphabet
    :message: encrypted message
    :return: estimated substitution alphabet
    """

    subs_alphabet = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return subs_alphabet




def uoc_custom_cipher(message):
    """
    EXERCISE 3.1: Custom cipher
    :message: message to cipher (plaintext)
    :return: ciphered text
    """

    ciphertext = ""

    #### IMPLEMENTATION GOES HERE ####
 

    # --------------------------------

    return ciphertext


def uoc_custom_decipher(message):
    """
    EXERCISE 3.2: Custom decipher
    :message: message to cipher (plaintext)
    :return: ciphered text
    """

    plaintext = ""

    #### IMPLEMENTATION GOES HERE ####


    # --------------------------------

    return plaintext










