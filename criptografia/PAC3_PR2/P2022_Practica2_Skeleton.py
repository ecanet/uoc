#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Eduard Canet
# @UOC 2021-2022 S2 Criptografia


# --- IMPLEMENTATION GOES HERE ---------------------------------------------
#  Student helpers (functions, constants, etc.) can be defined here, if needed
DEBUG=0



# --------------------------------------------------------------------------



def uoc_xor(str1, str2):
    """
    Implements an XOR operation.

    :param str1: string of 1 and 0s with the binary representation of the messsage
    :param str2: string of 1 and 0s with the binary representation of the messsage
    :return: tring of 1 and 0s with the binary representation of the result.
    """
	# Primerament iguala la mida de les cadenes binaries farcint zeros a l'esquerra de
	# la més curta. Després per a cada caràcter fa un xor matemàtic (en un altre exemple
	# està implementat amb un if/else).
	# Per fer el xor matemàtic converteix els caràcters a numeros i altre cop a chars usant
	# ord, chr i int

    xor = ""

    # --- IMPLEMENTATION GOES HERE ---
    zero=ord('0')
    l1=len(str1)
    l2=len(str2)
    if l1 < l2:
        str1=str1.zfill(l2)
    else:
        str2=str2.zfill(l1)
    for i in range(len(str1)):
        bit=int(str1[i],2)^int(str2[i])
        xor=xor+chr(bit+zero)
    # --------------------------------

    return xor


def uoc_congruential_generator(a, b, m, x):
    """
    Implements a congruential generator.

    :param a: the 'a' value of the generator.
    :param b: the 'b' value of the generator.
    :param m: module of the generator.
    :param x: initial value of the sequence
    :return: integer containing the next element of the sequence
    """
	# Implementa el generador congruencial amb l'operació matemàtica
	# (a * x + b) mòdul m

    next_value = 1

    # --- IMPLEMENTATION GOES HERE ---
    next_value=(a*x+b) % m

    # --------------------------------
    
    return next_value


def uoc_bad_stream_cipher(message, key):
    """
    Implements the Bad Stream Cipher.

    :param message: string of 1 and 0s with the binary representation of the messsage we want to encrypt or decrypt
    :param key: integer that represents the x_0 value of the generator
    :return: string of 1 and 0s with the binary representation of the ciphered or deciphered message
    """
    # Implementa el Bad Stream Cipher dividint el missatge de text pla en blocs de 8 bits, no verifica
    # que en sigui múltile, és un requsit de qui crida la funció. La funció utilitza paraules de 8 bits hardcoded.
    # Per a cada byte del missatge es fa el xor entre el missatge i byte del generador. Atenció en el primer
    # cas el primer valor usat de generador és directament la key.
    # Cal realitzar les conversions de tipus, el generadors i la key són enters i el missatge és un string de bits,
    # s'utilitza format per passar l'enter a un string binari. 
    # Cal tenir present que el generador pot generar valors majors que un byte i cal truncar-los.

    a = 3
    b = 7
    m = 2**8
    cipher_text = ""

    # --- IMPLEMENTATION GOES HERE ---
    DEBUG=0
    word=8
    generator=key
    msglist=[message[i:i+word] for i in range(0, len(message), word) ]
    if DEBUG: print(generator, msglist)
    for msg in msglist:
        xor=uoc_xor(msg,format(generator,'b'))
        binxor=xor[-word:]
        if DEBUG: print('g:', generator, 'msg:', msg, "xor:", xor, "binxor:", binxor)
        generator=uoc_congruential_generator(a, b, m, generator)
        cipher_text=cipher_text+binxor

    # --------------------------------

    return cipher_text


def uoc_calc_period(a, b, m, x_0):
    """
    Calculate the period of a congruential generator.

    :param x_0: integer that represents the initial value of the generator
    :param a: the 'a' value of the generator.
    :param b: the 'b' value of the generator.
    :param m: module of the generator.
    :return: period 
    """
	# Es tracta d'identificar quan el generador congruencial torna a generar el mateix valor rebut
	# com a llavor (X_0). S'itera infinitament i es surt del bucle quan es repeteix aquest valor.
	# Cal recirdar que el número d'elements és finit per tant sempre s'acabara repetint la seqüència.
	
    period = 0

    # --- IMPLEMENTATION GOES HERE ---
    g=x_0
    while True:
        period+=1
        g=uoc_congruential_generator(a, b, m, g)
        if DEBUG: print("n:", n, " g:", g)
        if g==x_0: break
    # --------------------------------

    return period




def uoc_flipping_attack(ciphertext):
    """
    Bit Flipping attack

    :param ciphertext: string of 1 and 0s with the binary representation of the captured ciphertext
    :return: list containing 4 ciphertext to reply
    """
	# Es tracta de generar vui versions dle ciphertext modificant els tres últims bits generant totes les
	# combinacions possibles, d'aquesta manera segur que una d'elles correspon un cop desxifrada al text
	# pla identificatiu de la parta 1.

    ciphertexts_to_reply = []

    # --- IMPLEMENTATION GOES HERE ---
    enditems=["000","001","010","011","100","101","110","111"]
    for item in enditems:
        ciphertexts_to_reply.append(ciphertext[:-3]+item)

    # --------------------------------

    return ciphertexts_to_reply


def uoc_my_stream_cipher(message, key):
    """
    Implements 2.6 

    :param message: string of 1 and 0s with the binary representation of the messsage we want to encrypt or decrypt
    :param key: integer that represents the x_0 value of the generator
    :return: string of 1 and 0s with the binary representation of the ciphered or deciphered message
    """
    # Implementa un CBC on primer es fa un xor del missatge amb el valor de l'estat anterior i del
    # resultat del xor s'aplica al generador congruencial.
    a = 3
    b = 7
    m = 2**8
    cipher_text = ""
    # --- IMPLEMENTATION GOES HERE ---
    DEBUG=0
    generator=key
    msglist=[message[i:i+8] for i in range(0, len(message), 8) ]
    if DEBUG: print(generator, msglist)
    for msg in msglist:
        xor=uoc_xor(msg,format(generator,'b'))
        generator=uoc_congruential_generator(a, b, m, int(xor,2))
        cipher_text=cipher_text+format(generator,'b')[-8:]
    # --------------------------------
    return cipher_text


if __name__ == '__main__':
	# Exemple d'implementació de l'exercici 2.5 / 2.6 per prevenir l'atac de bit flipping per 
	# obrir la porta 1.
	# Aquest codi imita el del Test i mostra que si s'altera l'ordre de la comanda
	# (primer la porta i després la acció) ja no es pot atacar una porta (si una acció)
	# El programa llista que tot i les 8 combinacions que s'intenten en l'atac de bit 
	# flipping, només la combinació original és vàlida.
	
    print("Implemnetació 2.5 ", "*"*70)
    for n in range(0,100):
        k = random.randint(2, 10000)
        opendoor_code = "10001000"
        door_id = "00000"+''.join([random.choice(["0", "1"]) for i in range(3)])
        cmd = uoc_bad_stream_cipher(door_id+opendoor_code, k)
        options = uoc_flipping_attack(cmd)
        for opt in options:
            plaintext = uoc_bad_stream_cipher(opt, k)
            if plaintext[:8]==door_id:
                if(plaintext[8:]==opendoor_code):
                    print("opendoor:", n, k, cmd, "=", opt)
                    break
            else:
                print("door_id:", n, k)
    print()







