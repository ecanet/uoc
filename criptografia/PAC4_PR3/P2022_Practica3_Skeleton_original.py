#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import math
import random
from sympy import randprime, isprime

# --- IMPLEMENTATION GOES HERE ---------------------------------------------
#  Student helpers (functions, constants, etc.) can be defined here, if needed




# --------------------------------------------------------------------------



def uoc_bezout(a, b):
    """
    Implements the Euclides Algorithm for finding the Bezout Identity:
    \lambda * a + \mu * b = d
    (See Theorem 2 and 3)

    :param a: integer value.
    :param b: integer value.
    :return: lambda and mu values
    """

    lamb = 1
    mu = 1
    # --- IMPLEMENTATION GOES HERE ---
    
    # --------------------------------

    return (lamb, mu)
    


def uoc_crt(a1, n1, a2, n2):
    """
    Implements the Chines remainder theorem using the uoc_bezout()
    function for solving the modular system:
    x = a1 mod n1
    x = a2 mod n2
    (See Theorem 7)

    :param a1: the 'a1' value of the modular system.
    :param n1: the 'n1' value of the modular system.
    :param a2: the 'a2' value of the modular system.
    :param n2: the 'n2' value of the modular system.
    :return: the 'x' value
    """

    x = 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------
    
    return x


def uoc_genkey(lamb):
    """
    Generate a random key for the proposed cryptosystem.

    :param lamb: lambda parameter
    :return: key values (N, d, p, q)
    """

    N, d, p, q = 1, 1, 1, 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------

    return (N, d, p, q)




def uoc_encrypt(m, N):
    """
    Encrypt the message using the proposed cryptosystem.

    :param m: integer containing the message to encrypt
    :return: the encrypted message
    """

    c = 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------

    return c


def uoc_decrypt(c, d, N):
    """
    Decrypt the message using the proposed cryptosystem.

    :param c: integer containing the message to decrypt
    :return: the decrypted message
    """

    m = 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------

    return m



def uoc_vote(votes, N):
    """
    Encrypt the votes using the proposed cryptosystem.

    :param votes: list of "YES", "NO" votes
    :return: encrypted votes
    """

    c = 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------

    return c


def uoc_vote_count(c, d, N):
    """
    Count the votes using the proposed cryptosystem.

    :param votes: list of "YES", "NO" votes
    :return: encrypted votes
    """

    count = 1

    # --- IMPLEMENTATION GOES HERE ---


    # --------------------------------

    return count





