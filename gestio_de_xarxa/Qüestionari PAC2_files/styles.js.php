
function local_ltiprovider_loadjscssfile(){
    var url = "http://moodos.uoc.edu/local/ltiprovider/styles.php?id=13341";
    var fileref=document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", url);
    var head = document.getElementsByTagName("head");
    if (head) {
        head[0].appendChild(fileref)
    }
}

// Waiting DOM ready (hide effect).
YUI().use('node', function(Y) {
      Y.on("domready", function(){
        local_ltiprovider_loadjscssfile();
      });
});

// Without waiting.
local_ltiprovider_loadjscssfile();